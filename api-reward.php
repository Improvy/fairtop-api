<?php
/* Скрипт выдачи награды за голосование на FairTop.in */
/* Version: 1.11 (12.02.2016) */

/* Конфигурация */
$conf = array(
	'mysql_host'  => 'localhost',                        // хост базы данных MySQL
	'mysql_user'  => 'user',                             // имя пользователя базы данных
	'mysql_pass'  => 'password',                         // пароль пользователя базы данных
	'mysql_db'    => 'iconomy',                          // название базы данных
	'mysql_table' => 'minecraft',                        // таблица базы данных
	'mysql_row'   => 'money',                            // столбец со значением денег и т.п.
	'mysql_u_row' => 'user',                             // столбец с логином игрока
	'open_key'    => 't0QrRMyQb2qNjdYP845qlcP3rHZvJPg3', // открытый ключ
	'secret_key'  => 'NzEPvQodydRyw3wwdls7sNOUYLwjAiwd', // закрытый ключ
	'money'       => 100,                                // количество денег, выдаваемых игроку за голосование
);
/* Конец конфигурации */

/* Выдача награды за голосование */ 
/*
	Изменение кода без знаний PHP и MySQL может вызвать непоправимые последствия:
	Ошибочное начисление денег, удаление таблицы из базы данных, ураганы, убийство котят и т.д.
	В случае, если Вы не можете самостоятельно настроить данный скрипт, пожалуйста, обратитесь в
	службу поддержки FairTop (https://fairtop.in/support)
*/
if (empty($_POST['player']) || empty($_POST['hash'])) {
	$return = array(
		'type' => 'error',
		'text' => 'Empty query parameter'
	);
	exit(json_encode($return));
}
$mysqli = mysqli_connect($conf['mysql_host'], $conf['mysql_user'], $conf['mysql_pass'], $conf['mysql_db']);
if (!$mysqli) {
	$return = array(
		'type' => 'error',
		'text' => 'MySQL Error: ('.mysqli_connect_errno().')'.mysqli_connect_error()
	);
	exit(json_encode($return));
}
$player = mysqli_real_escape_string($mysqli, $_POST['player']);
if ($_POST['hash'] != md5(sha1($player.':'.$conf['secret_key']))) {
	$return = array(
		'type' => 'error',
		'text' => 'Incorrect hash'
	);
	exit(json_encode($return));
}
$table = $conf['mysql_table'];
$row   = $conf['mysql_row'];
$urow  = $conf['mysql_u_row'];
$money = $conf['money'];
mysqli_query($mysqli, "UPDATE `$table` SET `$row`=`$row`+'$money' WHERE `$urow`='$player';");
if (mysqli_error($mysqli)) {
	$return = array(
		'type' => 'error',
		'text' => mysqli_error($mysqli)
	);
	exit(json_encode($return));
}
$return = array(
	'type' => 'success',
	'text' => 'Updated'
);
exit(json_encode($return));
/* Конец выдачи награды */
?>
